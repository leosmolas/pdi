#ifndef MDISUBWINDOW_H
#define MDISUBWINDOW_H

#include <QMdiSubWindow>

class MdiSubWindow : public QMdiSubWindow
{
    Q_OBJECT
public:
    explicit MdiSubWindow(QWidget *parent = 0);
    explicit MdiSubWindow(QImage im, QWidget *parent = 0);

    QImage im;
signals:

public slots:

};

#endif // MDISUBWINDOW_H
