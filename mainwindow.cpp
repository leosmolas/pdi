#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QImage>
#include <QMessageBox>
#include <QMdiSubWindow>
#include "mdiimage.h"
#include "mdisubwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->mdiArea->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAbrir_imagen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Abrir Imagen"), "~", tr("Archivos de imagen (*.png *.jpg *.bmp)"));
    if (!fileName.isEmpty()) {
        QImage image(fileName);
        if (image.isNull()) {
            QMessageBox::information(this, tr("PDI"),
                                     tr("No se puede cargar %1.").arg(fileName));
            return;
        }
        MdiSubWindow *subWindow = new MdiSubWindow(image);

        ui->mdiArea->addSubWindow(subWindow);

        subWindow->show();
    }
}
