#-------------------------------------------------
#
# Project created by QtCreator 2014-06-18T18:14:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pdi
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mdiimage.cpp \
    mdisubwindow.cpp

HEADERS  += mainwindow.h \
    mdiimage.h \
    mdisubwindow.h

FORMS    += mainwindow.ui \
    mdiimage.ui
