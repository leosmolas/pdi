#include "mdisubwindow.h"
#include "mdiimage.h"

MdiSubWindow::MdiSubWindow(QWidget *parent) :
    QMdiSubWindow(parent)
{
}

MdiSubWindow::MdiSubWindow(QImage im, QWidget *parent) :
    QMdiSubWindow(parent), im(im)
{
    MdiImage *mdiImage = new MdiImage(im);
    this->setWidget(mdiImage);
}
