#include "mdiimage.h"
#include "ui_mdiimage.h"

MdiImage::MdiImage(QWidget *parent) :
    QFrame(parent),
    ui(new Ui::MdiImage)
{
    ui->setupUi(this);
}

MdiImage::MdiImage(QImage im, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::MdiImage)
{
    ui->setupUi(this);
    QGraphicsScene *scene = new QGraphicsScene();
    image = QPixmap::fromImage(im);
    scene->addPixmap(image);
    ui->graphicsView->setScene(scene);
    ui->graphicsView->show();   
}


MdiImage::~MdiImage()
{
    delete ui;
}

