#ifndef MDIIMAGE_H
#define MDIIMAGE_H

#include <QFrame>

namespace Ui {
class MdiImage;
}

class MdiImage : public QFrame
{
    Q_OBJECT

public:
    explicit MdiImage(QWidget *parent = 0);
    explicit MdiImage(QImage im, QWidget *parent = 0);
    ~MdiImage();

    QPixmap image;
private:
    Ui::MdiImage *ui;
};

#endif // MDIIMAGE_H
